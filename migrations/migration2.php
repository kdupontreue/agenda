<?php

require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/connexion.php';

$queries = [
    'ALTER TABLE task_user ADD CONSTRAINT PK_task_id PRIMARY KEY (task_id)',
    'ALTER TABLE task_user ADD CONSTRAINT PK_task_user_id PRIMARY KEY (user_id)',
    'ALTER TABLE calendar_user ADD CONSTRAINT PK_calendar_id PRIMARY KEY (calendar_id)',
    'ALTER TABLE calendar_user ADD CONSTRAINT PK_calendar_user_id PRIMARY KEY (user_id)',
    'ALTER TABLE calendars ADD CONSTRAINT FK_calendars_owner FOREIGN KEY (owner) REFERENCES users (id)',
    'ALTER TABLE task_user ADD CONSTRAINT FK_task_id FOREIGN KEY (task_id) REFERENCES tasks (id)',
    'ALTER TABLE task_user ADD CONSTRAINT FK_task_user_id FOREIGN KEY (user_id) REFERENCES users (id)',
    'ALTER TABLE calendar_user ADD CONSTRAINT FK_calendar_id FOREIGN KEY (calendar_id) REFERENCES calendars (id)',
    'ALTER TABLE calendar_user ADD CONSTRAINT FK_calendar_user_id FOREIGN KEY (user_id) REFERENCES users (id)',
    'ALTER TABLE tasks ADD CONSTRAINT FK_tasks_owner FOREIGN KEY (owner) REFERENCES users (id)',
    'ALTER TABLE tasks ADD CONSTRAINT FK_tasks_calendar_id FOREIGN KEY (calendar_id) REFERENCES calendars (id)',
];

foreach ($queries as $query) {
    try {
        $statement = $connection->prepare($query);
        $statement->execute();
    } catch (Exception $e) {
        echo 'Erreur : ' . $e->getMessage() .'<br/>';
    }
}