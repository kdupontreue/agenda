<?php

require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/connexion.php';

$queries = [
    'CREATE TABLE IF NOT EXISTS users (id int(11) PRIMARY KEY AUTO_INCREMENT, lastname varchar(50) NOT NULL, firstname varchar(50) NOT NULL, email varchar(320) NOT NULL UNIQUE, password varchar(255) NOT NULL, created_at datetime NOT NULL default CURRENT_TIMESTAMP)',
    'CREATE TABLE IF NOT EXISTS calendars (id int(11) PRIMARY KEY AUTO_INCREMENT, name varchar(50) NOT NULL, is_public tinyint(1) NOT NULL, owner int(11) NOT NULL, created_at datetime NOT NULL default CURRENT_TIMESTAMP)',
    'CREATE TABLE IF NOT EXISTS tasks (id int(11) PRIMARY KEY AUTO_INCREMENT, title varchar(50) NOT NULL, date_start date NOT NULL, date_end date, hour_start time, hour_end time, location varchar(255), description text, type varchar(20) NOT NULL, owner int(11) NOT NULL, calendar_id int(11) NOT NULL, created_at datetime NOT NULL default CURRENT_TIMESTAMP)',
    'CREATE TABLE IF NOT EXISTS task_user (task_id int(11) NOT NULL, user_id int(11) NOT NULL)',
    'CREATE TABLE IF NOT EXISTS calendar_user (calendar_id int(11) NOT NULL, user_id int(11) NOT NULL)'
];

foreach ($queries as $query) {
    try {
        $statement = $connection->prepare($query);
        $statement->execute();
    } catch (Exception $e) {
        echo 'Erreur : ' . $e->getMessage() .'<br/>';
    }
}