<?php

$url = explode('/', $_SERVER['REQUEST_URI']);
if($url[2] === 'month' || empty($url[2])) {
    $displayType = "components/month_card.html.twig";
    $selectedMonth = 'selected';
}

require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/controllers/calendarComponentController.php';

require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/controllers/calendarController.php';
