<?php

require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/models/userModel.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/validator.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR .'/views');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);

if(!empty($_POST['email']) && !empty($_POST['password'])) {

    $valide =  $valide = validate([
        "email" => ['required', 'max320', 'is_email'],
        "password" => ['required', 'is_password'],
    ]);

    if($valide) {
        $email = strip_tags($_POST['email']);
        $password = $_POST['password'];

        $user = getUserByEmail($email);

        if(password_verify($password, $user['password'])) {
           
            setcookie('user_id', $user['id'], time() +86400, '/', false, false);
            
            header('Location: /');
            exit();
        } else {
            echo 'Le nom d\'utilisateur ou le mot de passe sont incorrects.';
            $template = $twig->load('authView.html.twig');
            echo $template->render([
            ]);
        }
    }        
} else {
    $template = $twig->load('authView.html.twig');
    echo $template->render([
    ]);
}
