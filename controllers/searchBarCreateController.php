<?php
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'middleware/authentication.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/models/calendarModel.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/validator.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR .'/views');
$twig = new \Twig\Environment($loader, ['cache' => false,]);


if (isset($_GET['search'])){
    $validate =  validate([ "email" => ['max320', 'is_email']]);

    if($validate){
        $email=$_GET['search'];
        searchEmailByEmail($email);
        $emails[]=$email;
    }
}

$template = $twig->load('createCalendarView.html.twig');
echo $template->render(['user' => $user,
//    'email'=> $emails,
//                        'nameCalendar' => $nameCalendar,
//                        'choiceCalendar'=> $choiceCalendar,
]);

