<?php

session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . '/vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR .'/views');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);

setcookie('user_id', '', time()-3600, '/', false, false);

header('location: /auth');