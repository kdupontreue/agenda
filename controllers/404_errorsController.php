<?php

require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR .'/views');
$twig = new \Twig\Environment($loader, ['cache' => false,]);

$error = 'ceci est une erreur';

$template = $twig->load('404_errors.html.twig');

echo $template->render([]);
