<?php

$url = explode('/', $_SERVER['REQUEST_URI']);
if($url[2] === 'week') {
    $displayType = "components/week_card.html.twig";
    $selectedWeek = 'selected';
}

require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/controllers/calendarController.php';
