<?php

/** @var array $user */
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'middleware/authentication.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/models/userModel.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/controllers/calendarComponentController.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/vendor/autoload.php';



$loader = new \Twig\Loader\FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR .'/views');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);

$template = $twig->load('home.html.twig');
echo $template->render(['user' => $user,
                        'daysOfWeek' => $daysOfWeek,
                        'numberDays' =>$numberDays,
]);
