<?php

/** @var array $user */
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'middleware/authentication.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/controllers/calendarComponentController.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/models/userModel.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/models/calendarModel.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/validator.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR .'/views');
$twig = new \Twig\Environment($loader, ['cache' => false,]);

if (isset($_POST['calendar-name'])) {

    $validate = validate(["calendar-name" => ['required', 'max50']]);
    $selectRadio =(int) $_POST['calendrier'] ?? '';

    if (isset($selectRadio)) {

        if ($selectRadio === 1 || $selectRadio === 0) {

            if ($validate) {

                $owner = $user['id'];
                $calendar = strip_tags($_POST['calendar-name']);
                $is_public = $selectRadio;

                createCalendar($calendar, $is_public, $owner);

//                $template = $twig->load('home.html.twig');
//                echo $template->render(['user' => $user,
//                                        'daysOfWeek' => $daysOfWeek,
//                                        'numberDays' =>$numberDays,
//                ]);

            }
        } else {
            $choiceCalendar = "Merci de choisir entre calendrier public et calendrier privée";
        }
    }
}else {
    $nameCalendar= "merci de renseigner le nom de calendrier avec un maximum de 50 caractères";
}

$template = $twig->load('createCalendarView.html.twig');

echo $template->render(['user' => $user,
//                        'nameCalendar' => $nameCalendar,
//                        'choiceCalendar'=> $choiceCalendar,
]);
