<?php

use Carbon\Carbon;

require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/vendor/autoload.php';

// -- initialisation des jours de la semaine
$calendar = Carbon::create('2022-02-20');

for($i=0; $i<7 ; $i++){
    $daysOfWeek[]=$calendar->addDay()->locale('FR')->dayName;
}

// --calcule du nombre de jours selon le mois
$today = Carbon::now()->locale('FR');
$lastDayOfMonth = $today->endOfMonth()->day;

$quantiemes = range(1,  $lastDayOfMonth);

// --commencer le mois
$firstDayOfMonth = $today->firstOfMonth()->dayOfWeek;
$firstDayOfWeek = $today->firstWeekDay;
$lastDayOfSubMonth = $today->subMonth()->lastOfMonth()->day;

for($firstDayOfWeek=1; $firstDayOfWeek<$firstDayOfMonth; $firstDayOfWeek++){
    $subMonth[]=$lastDayOfSubMonth--;
}

// --fin du mois
$firstDayOfAddMonth = $today->addMonth()->firstOfMonth()->dayOfWeek;
$lastDayOfWeek = 7;
$firstDayOfWeek = 1;

for($firstDayOfAddMonth; $firstDayOfAddMonth <=$lastDayOfWeek; $firstDayOfAddMonth++){
    $addMonth[]=$firstDayOfWeek++;
}

// --tableau complet calendrier
$numberDays= array_merge(array_reverse($subMonth), $quantiemes, $addMonth);
