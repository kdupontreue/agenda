<?php

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Twig\Environment;
use Twig\Extension\CoreExtension;
use Twig\Loader\FilesystemLoader;
use Twig\Extra\Intl\IntlExtension;

/** @var array $user */
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'middleware/authentication.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/controllers/calendarComponentController.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/models/userModel.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/vendor/autoload.php';
///** @var string $displayType */
//require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'controllers/weekCalendarController.php';
///** @var string $displayType */
//require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'controllers/dayCalendarController.php';
///** @var string $displayType */
//require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'controllers/monthCalendarController.php';

$loader = new FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR .'/views');
$twig = new Environment($loader, [
    'cache' => false,
]);
$twig->getExtension(CoreExtension::class)->setTimezone('Europe/Paris');
$twig->addExtension(new IntlExtension());

if(empty($selectedDay)) $selectedDay = '';
if(empty($selectedWeek)) $selectedWeek = '';
if(empty($selectedMonth)) $selectedMonth = '';
$url = explode('/', $_SERVER['REQUEST_URI']);
(isset($url[2]))? $type = $url[2] : $type = 'month';
$displayType = "components/".$type."_card.html.twig";

$today = Carbon::now()->locale('fr_FR');
$yearActual = $today->year;
$period = 10;
$yearMax = $yearActual + $period;
$months = CarbonPeriod::create("$yearActual-01", '1month', "$yearActual-12");
$monthTargeted = $today->month;
$yearTargeted = $yearActual;

if(isset($_GET['month']) && !empty($_GET['month'])) {
    $monthTargeted = $today->setMonth($_GET['month']);
}
if(isset($_GET['year']) && !empty($_GET['year'])) {
    $yearTargeted = $today->setYear($_GET['year']);
}

$template = $twig->load('calendarView.html.twig');
/** @var TYPE_NAME $daysOfWeek */
/** @var TYPE_NAME $selectedWeek */
/** @var TYPE_NAME $selectedMonth */
/** @var TYPE_NAME $selectedDay */
/** @var TYPE_NAME $numberDays */
echo $template->render(['user' => $user,
                        'today' => $today,
                        'months' => $months,
                        'yearActual' => $yearActual,
                        'yearMax' => $yearMax,
                        'displayType' => $displayType,
                        'selectedDay' => $selectedDay,
                        'selectedMonth' => $selectedMonth,
                        'selectedWeek' => $selectedWeek,
                        'daysOfWeek' => $daysOfWeek,
                        'numberDays' =>$numberDays,
                        'type' => $type,
                        'monthTargeted' => $monthTargeted,
                        'yearTargeted' => $yearTargeted
]);
