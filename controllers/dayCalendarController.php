<?php

$url = explode('/', $_SERVER['REQUEST_URI']);
if($url[2] === 'day') {
    $displayType = "components/day_card.html.twig";
    $selectedDay = 'selected';
}

require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/controllers/calendarController.php';
