<?php

require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/models/userModel.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/validator.php';
require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR .'/views');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);

if(!empty($_POST['lastname']) && !empty($_POST['firstname']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['password_confirmation'])) {

    $validate = validate([
        "lastname" => ['required', 'max50'],
        "firstname" => ['required', 'max50'],
        "email" => ['required', 'max320', 'is_email'],
        "password" => ['required', 'is_password', 'max255'],
        "password_confirmation" => ['required', 'is_password', 'max255'],
    ]);

    if($validate) {
        if($_POST['password'] === $_POST['password_confirmation']){
            $lastname = strip_tags($_POST['lastname']);
            $firstname = strip_tags($_POST['firstname']);
            $email = strip_tags($_POST['email']);
            $password = password_hash($_POST['password'], PASSWORD_ARGON2I);
            
            $emailExists = emailExists($email);
            if($emailExists) {
                echo "L'Email " . $emailExists['email'] . " existe déjà !";
                    $template = $twig->load('registerView.html.twig');
            }else {
                createUser($lastname, $firstname, $email, $password);
                    $template = $twig->load('authView.html.twig');
            }
        }else {
            echo "Les deus mots de passe ne correspondent pas !";
            $template = $twig->load('registerView.html.twig');
        }
        echo $template->render([
        ]);
    }
}

$template = $twig->load('registerView.html.twig');
echo $template->render([
]);
