<?php

/**
 * @param string $calendar
 * @param bool $is_public
 * @param string $owner
 * @return void
 */
function createCalendar(string $calendar, bool $is_public, string $owner): void
{
    /** @var PDO $connection */
    require $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'connexion.php';
    $sql = 'INSERT INTO  calendars (name, is_public, owner) VALUES (:calendar, :is_public, :owner)';
    $query = $connection->prepare($sql);
    $query ->bindValue (':calendar', $calendar, PDO::PARAM_STR);
    $query ->bindValue (':is_public', $is_public, PDO::PARAM_BOOL);
    $query ->bindValue (':owner', $owner, PDO::PARAM_INT);
    $query->execute();
}

function searchEmailByEmail($email){
    /** @var PDO $connection */
    require $_SERVER['DOCUMENT_ROOT'] .  DIRECTORY_SEPARATOR . 'connexion.php';
    $sql = 'SELECT email FROM users where email = :email';
    $query = $connection->prepare($sql);
    $query ->bindValue(':email', $email, PDO::PARAM_STR);
    $query->execute();
    $email = $query->fetch();

    return $email;
}


