<?php

/**
 * @param string $lastname
 * @param string $firstname
 * @param string $email
 * @param string $password
 * @return void
 */
function createUser(string $lastname, string $firstname, string $email, string $password): void {
    /** @var PDO $connection */
    require $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'connexion.php';

    $sql = 'INSERT INTO users (lastname, firstname, email, password) VALUES (:lastname, :firstname, :email, :password)';

    $query = $connection->prepare($sql);
    $query->bindValue(':lastname', $lastname, PDO::PARAM_STR);
    $query->bindValue(':firstname', $firstname, PDO::PARAM_STR);
    $query->bindValue(':email', $email, PDO::PARAM_STR);
    $query->bindValue(':password', $password, PDO::PARAM_STR);
    $query->execute();
}

/**
 * @param string $email
 * @return bool
 */
function emailExists(string $email): bool {
    /** @var PDO $connection */
    require $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'connexion.php';

    $sql = 'SELECT email FROM users WHERE email=:email';

    $query = $connection->prepare($sql);
    $query->bindValue(':email', $email, PDO::PARAM_STR);
    $query->execute();
    $email = $query->fetch();

    return $email;
}

/**
 * @param $email
 * @return array
 */
function getUserByEmail($email): array {
    /** @var PDO $connection */
    require $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'connexion.php';

    $sql = 'SELECT * FROM users WHERE email=:email';

    $query = $connection->prepare($sql);
    $query->bindValue(':email', $email, PDO::PARAM_STR);
    $query->execute();
    $user = $query->fetch();
    return $user;
}

/**
 * @param int $user_id
 * @return array
 */
function getUserById(int $user_id): array {
    /** @var PDO $connection */
    require($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'connexion.php');
    $sql = 'SELECT * FROM users WHERE id = :id';
    $query = $connection->prepare($sql);
    $query->bindValue(':id', $user_id);
    $query->execute();
    $user = $query->fetch();
    return $user;
}