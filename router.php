<?php

$uri = $_SERVER['REQUEST_URI'];

const ROUTING = [
    "" => 'controllers/homeController.php',
    "/errors" => 'controllers/404_errorsController.php',
    "/register" => 'controllers/registerController.php',
    "/auth" => 'controllers/authController.php',
    "/disconnect" => 'controllers/disconnectController.php',
    "/create-calendar" => 'controllers/createCalendarController.php',
    "/calendar" => 'controllers/monthCalendarController.php',
    "/calendar/day" => 'controllers/dayCalendarController.php',
    "/calendar/week" => 'controllers/weekCalendarController.php',
    "/calendar/month" => 'controllers/monthCalendarController.php',
    "/searchCreate" => 'controllers/searchBarCreateController.php',

];

function getMatchAndParamFromUri(string $uri): array
{
    $path = preg_split('#/#', $uri);

    $matching = "";

    $param = "";

    foreach ($path as $p){

        if(is_numeric($p)) {
            
            $param=$p;

            $p='*';

        }
        $matching .=$p .'/';
    }

    return [rtrim($matching, '/'), $param];

}

function getErrorIfNotExists(string $matching): string{

    if(!array_key_exists($matching, ROUTING)){
        $matching = "/errors";
    }
    return $matching;

}

list($matching, $param)= getMatchAndParamFromUri($uri);

$matching = GetErrorIfNotExists($matching);

include ROUTING[$matching];
