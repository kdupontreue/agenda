<?php

// validate([
//     "lastname" => ['required', 'max50'],
//     "firstname" => ['required', 'max50'],
//     "email" => ['required', 'max320', 'is_email'],
//     "password" => ['required', 'is_password', 'max255'],
//     "title" => ['required', 'max50'],
//     "date_start" => ['required', 'is_valid_start_date'],
//     "date_end" => ['is_valid_end_date'],
//     "hour_start" => ['is_valid_time'],
//     "hour_end" => ['is_valid_time'],
//     "location" => ['max255'],
//     "name" => ['required', 'max50'],
//     "is_public" => ['required', 'is_boolean']
// ]);

function validateDate($date, $format) {
    $d = DateTime::createFromFormat($format, $date);
    return $d->format($format) == $date;
}
/* Quatre fonctions à finir d'implémenter quand on aura les formulaires correspondants
pour connaitre les formats de données récupérés */
function validate($validation): bool {
    $valide = true;
    foreach ($validation as $fieldname => $constraints) {
        if($valide == false) {
            break;
        }
        foreach($constraints as $constraint) {
            if($valide == false) {
                break;
            }
            switch ($constraint) {
                case 'required': if(!empty($fieldname)) $valide = true;
                break;
                case 'max50': if(strlen($fieldname) <= 50 && isset($fieldname)) $valide = true;
                break;
                case 'max255': if(strlen($fieldname) <= 255 && isset($fieldname)) $valide = true;
                break;
                case 'max320': if(strlen($fieldname) <= 320 && isset($fieldname)) $valide = true;
                break;
                case 'is_email': if(filter_var($fieldname, FILTER_VALIDATE_EMAIL) && isset($fieldname)) $valide = true;
                break;    
                case 'is_password': if(preg_match('^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*^', $fieldname)) $valide = true;
                break;
                case 'is_valid_start_date': if(validateDate($fieldname, 'Y-m-d') && strtotime($fieldname) >= strtotime('now') && isset($fieldname)) $valide = true;
                break;
                case 'is_valid_end_date': if(validateDate($fieldname, 'Y-m-d') && strtotime($fieldname) >= strtotime("date_start") && isset($fieldname)) $valide = true;
                break;
                case 'is_valid_time': if(validateDate($fieldname, 'H:i') && isset($fieldname)) $valide = true;
                break;
                default: $valide = false;
                break;
            }
        }
    }
    return $valide;
}
