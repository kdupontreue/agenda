<?php

session_start();

require_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR .'/models/userModel.php';

$user = [];
if(isset($_COOKIE['user_id'])){
    $user = getUserById($_COOKIE['user_id']);
}

function isConnectedUser(array $user): void {
    if(empty($user)){
        $_SESSION['message'] = "merci de vous connecter !";
        header("Location: /auth");
        exit();
    }
}

isConnectedUser($user);
